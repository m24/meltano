---
metaTitle: Meltano Support & Help
description: Learn where to file bugs, track issues, and how to get support from the Meltano Slack and other channels.
---

# Getting Help

We would love to help, and you can reach us anytime at `hello@meltano.com`.

## Feedback / Bugs

If you ran into a bug or other problem or have any kind of feedback, you can:

1. Check out the [Meltano issue tracker][issues] to see if someone else has already reported the same issue or made the same request. Feel free to weigh in with extra information, or just to make sure the issue gets the attention it deserves.
2. Join the <SlackChannelLink>Meltano Slack channel</SlackChannelLink> which is frequented by the team and the rest of the community. You can ask any questions you may have in here, or just chat with fellow users.
3. <IntercomLink>Contact the team directly</IntercomLink> using Intercom, right from this website.
4. File a new issue on the [Meltano issue tracker][issues] so that we can track your problem or suggestion and find the best solution together.

[issues]: https://gitlab.com/meltano/meltano/issues/

## Delete Your Data

If you would like to delete your data from MeltanoData.com, we are more than happy to assist. All you have to do is send us an email at `hello@meltano.com` and we will get that taken care for you!
