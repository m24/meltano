---
home: true
heroImage: /meltano-logo.svg
primaryAction:
  text: Start 30 Day Free Trial
  link: https://meltano.typeform.com/to/NJPwxv
secondaryAction:
  text: Live Demo
  link: https://meltano.meltanodata.com/
metaTitle: Meltano is an open source data dashboarding tool from GitLab
description: Meltano provides an integrated workflow for modeling, extracting, loading, transforming, analyzing, notebooking, and orchestrating your data.
---
